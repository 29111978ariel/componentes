import styled from 'styled-components';
import Menu from './menu';
// import Option, { params } from './menu/option';

const Container = styled.div`
  /* background-color: blue; */
`

const App = (): JSX.Element => {

  // const options: string[] = [
  //   '1up nutrition',
  //   'asitis',
  //   'avvatar',
  //   'big muscle',
  //   'bpi sports',
  //   'bsn',
  //   'cellucor',
  //   'domin8r',
  //   'dymatize',
  // ];

  // const handleClick: params['onClick'] = (element) => {
  //   console.log(element);
  // }

  return (
    <Container>
      {/* {options.map((value, index) => <Option key={index} value={value} onClick={handleClick} />)} */}
      <Menu 
        onCheck={e => console.log(e)} 
        onCancel={() => console.log("SELECCIÓN CANCELADA")} 
      />
      <Menu 
        options={['Price low to high', 'Price high to low', 'Popularity']} 
        backgroundColor={'#21d0d0'} 
        onCheck={e => console.log(e)} 
        onCancel={() => console.log("SELECCIÓN CANCELADA")} 
      />
    </Container>
  )
};

export default App;
