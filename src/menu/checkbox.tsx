import styled from 'styled-components';
// import check from '../../assets/iconos/check.svg';

const Container = styled.div<{ visible: boolean }>`
border-radius: 50%;
border: 1.5px solid white;
height: 28px;
width: 28px;
cursor: pointer;
background-color: ${props => props.visible ? 'white' : 'transparent'};
`

const Icon = styled.img<{ visible: boolean }>`
height: 28px;
width: 28px;
visibility: ${props => props.visible ? 'visible' : 'hidden'};
`
export interface params {
  image: string;
  onClick?: (value: boolean) => void
  state: boolean;
}

const CheckBox = (params: params): JSX.Element => {

  const handleClick = () => {
    if (typeof params.onClick === 'function') params.onClick(!params.state);
  }
  return (
    <Container visible={params.state} onClick={handleClick}>
      <Icon src={params.image} visible={params.state} />
    </Container>
  )
}

CheckBox.defaultProps = {
  // state: false,
}
export default CheckBox;
