import { useState } from 'react';
import styled from 'styled-components';
// import check from '../../assets/iconos/check.svg';

const Container = styled.div`
border-radius: 50%;
border: 1.5px solid white;
height: 48px;
width: 48px;
cursor: pointer;
background-color: white;
`

const Icon = styled.img`
height: 100%;
width: 100%;
`
export interface params {
  image: string;
  onClick?: () => void
}

const CheckBox = (params: params): JSX.Element => {

  const handleClick = () => {
    if (typeof params.onClick === 'function') params.onClick();
  }
  return (
    <Container onClick={handleClick}>
      <Icon src={params.image} />
    </Container>
  )
}

export default CheckBox;
