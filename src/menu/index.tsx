import { useEffect, useState } from 'react';
import styled from 'styled-components';
import Footer from './footer';
import Option, { params as paramsComponentOption } from './option';

const Container = styled.div<{ color: string }>`
  position: relative;
  background-color: ${props => props.color};
  margin-bottom: 48px;
  padding: 12px 14px 8px;
  border-radius: 24px;
`

export interface params {
  options: string[];
  backgroundColor: string;
  onCheck?: (options: option[]) => void;
  onCancel?: () => void;
}

export interface option {
  value: string;
  state: boolean;
}

const Menu = (params: params): JSX.Element => {

  // const options: string[] = [
  //   '1up nutrition',
  //   'asitis',
  //   'avvatar',
  //   'big muscle',
  //   'bpi sports',
  //   'bsn',
  //   'cellucor',
  //   'domin8r',
  //   'dymatize',
  // ];

  const [options, setOptions] = useState<option[]>([])

  useEffect(() => {
    const raw: option[] = [];
    params.options.forEach(element => raw.push({ value: element, state: false }));
    setOptions(raw);

    // return () => {
    //   second
    // };
  }, [params.options])


  const handleClick: paramsComponentOption['onClick'] = (element) => {
    const copy: option[] = [...options];
    const index: number = copy.findIndex(item => item.value === element.value);

    if (index > -1) copy[index] = element;

    setOptions(copy);
  }
  const handleCheck = () => {
    if (typeof (params.onCheck) === 'function') params.onCheck(options);
  }
  const handleReset = () => {
    const raw: option[] = [];
    params.options.forEach(element => raw.push({ value: element, state: false }));
    setOptions(raw);
    }
    const handleCancel = () => {
      if (typeof (params.onCancel) === 'function') params.onCancel();
    }

  return (
    <Container color={params.backgroundColor}>
      {options.map((value, index) => <Option key={index} state={value.state} value={value.value} onClick={handleClick} />)}
      <Footer onCheck={handleCheck} onReset={handleReset} onCancel= {handleCancel} />
    </Container>
  )
};

Menu.defaultProps = {
  options: [
    '1up nutrition',
    'asitis',
    'avvatar',
    'big muscle',
    'bpi sports',
    'bsn',
    'cellucor',
    'domin8r',
    // 'dymatize',
  ],
  backgroundColor: '#ff7745'
}

export default Menu;
