import styled from 'styled-components';

import Button from './btn';
import check from '../assets/iconos/check.svg';
import cancel from '../assets/iconos/cancel.svg';
import reset from '../assets/iconos/reset.svg';

const ContainerV1 = styled.div`
  position: relative;
  bottom: -32px;
  display: flex;
  justify-content: center;
  gap: 16px;
`;
const ContainerV2 = styled.div`
  display: flex;
  justify-content: center;
  gap: 16px;
  margin-top: 12px;
  padding: 20px 0;
  border-top: 1px solid #ffffff80;
`;

interface option {
  icon: string;
  onClick: () => void;
};

export interface params {
  enableReset?: boolean;
  enableCancel?: boolean;
  onReset?: () => void;
  onCheck?: () => void;
  onCancel?: () => void;
}

const Footer = (params: params): JSX.Element => {

  const handleReset = () => {
    console.log('Reset');
    if (typeof params.onReset === 'function') params.onReset();
  };
  const handleCheck = () => {
    // console.log('Check');
    if (typeof params.onCheck === 'function') params.onCheck();
  };
  const handleCancel = () => {
    // console.log('Cancel');
    if (typeof params.onCancel === 'function') params.onCancel();
  };

  const data: option[] = [
    {
      icon: check,
      onClick: handleCheck,
    },
  ];

  if (params.enableReset) data.push({ icon: reset, onClick: handleReset });
  if (params.enableCancel) data.push({ icon: cancel, onClick: handleCancel });

  return (
    <ContainerV2>
      {data.map((value, index) => (
        <Button key={index} onClick={value.onClick} image={value.icon} />
      ))}
    </ContainerV2>
  )
}

Footer.defaultProps = {
  enableReset: true,
  enableCancel: true,
}

export default Footer
