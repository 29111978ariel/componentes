import styled from "styled-components";

import check from '../assets/iconos/check.svg';
import CheckBox from "./checkbox";

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 13px;
`;

const Title = styled.p`
  margin: 0;
  font-size: 13px;
  font-weight: 500;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  text-transform: uppercase;
  color: white;
`;


export interface params {
  value: string;
  state: boolean;
  onClick?: (params: { value: string, state: boolean }) => void;
}

const Option = (params: params): JSX.Element => {

  const handleClick = (state: boolean) => {
    if (typeof params.onClick === 'function') params.onClick({ value: params.value, state });
  }

  return (
    <Container>
      <Title>
        {params.value}
      </Title>
      <CheckBox image={check} state={params.state} onClick={handleClick} />

    </Container>
  )
}

export default Option